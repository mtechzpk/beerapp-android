package com.example.beershop.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beershop.R;

public class BillSecondFragment extends Fragment {
    public BillSecondFragment() {
        // Required empty public constructor
    }
    private View view;
    private ImageView back;
    private TextView total_beer, total_price, view_details;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bill_second, container, false);

        init();
        return view;
    }

    private void init() {
        back = view.findViewById(R.id.back);

        total_beer = view.findViewById(R.id.total_beer);
        total_price = view.findViewById(R.id.total_price);
        view_details = view.findViewById(R.id.view_details);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "View Details", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
