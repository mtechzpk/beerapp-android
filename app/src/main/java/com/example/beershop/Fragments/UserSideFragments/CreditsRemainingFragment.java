package com.example.beershop.Fragments.UserSideFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.beershop.Adapters.CreditsReceivedAdapter;
import com.example.beershop.Adapters.CreditsRemainingAdapter;
import com.example.beershop.Models.CreditsModel;
import com.example.beershop.R;

import java.util.ArrayList;

public class CreditsRemainingFragment extends Fragment {
    public CreditsRemainingFragment() {
        // Required empty public constructor
    }
    private View v;
    private ImageView back_go;
    private RecyclerView credits_remaining_rv;
    private ArrayList<CreditsModel> creditsModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v =  inflater.inflate(R.layout.fragment_credits_remaining, container, false);
        init();

        return v;
    }

    private void init() {
        back_go = v.findViewById(R.id.back_go);
        credits_remaining_rv = v.findViewById(R.id.credits_remaining_rv);
        initAdapter();

        back_go.setOnClickListener(Navigation.
                createNavigateOnClickListener(R.id.action_creditsRemainingFragment_to_mainCustomerFragment));

    }

    private void setEventsName() {
        creditsModel = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            CreditsModel viewDetailsModel = new CreditsModel();
            viewDetailsModel.setCredit_name("Survey" + i);
            viewDetailsModel.setCredit_date("03" + i);
            viewDetailsModel.setCredit_time("04:30" + i);
            creditsModel.add(viewDetailsModel);
        }
    }

    private void initAdapter() {
        setEventsName();
        CreditsRemainingAdapter pAdapter = new CreditsRemainingAdapter(getActivity(), creditsModel);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        credits_remaining_rv.setLayoutManager(linearLayoutManager);
        credits_remaining_rv.setAdapter(pAdapter);
    }
}
