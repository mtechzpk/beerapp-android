package com.example.beershop.Fragments.UserSideFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beershop.R;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.button.MaterialButton;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileFragment extends Fragment {
    public UserProfileFragment() {
        // Required empty public constructor
    }

    private View v;
    private CircleImageView profile_img;
    private TextView change_image;
    private EditText user_name,user_email,user_pass;
    private MaterialButton btn_save;
    private ImageView edit_name,edit_email,edit_pass, back_ic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_user_profile, container, false);

        init();
        return v;
    }

    private void init() {
        change_image = v.findViewById(R.id.upload);
        profile_img = v.findViewById(R.id.profile_img);

        edit_name = v.findViewById(R.id.edit_name);
        edit_email = v.findViewById(R.id.edit_email);
        edit_pass = v.findViewById(R.id.edit_pass);
        back_ic = v.findViewById(R.id.back);

        user_name = v.findViewById(R.id.user_name);
        user_email = v.findViewById(R.id.user_email);
        user_pass = v.findViewById(R.id.user_pass);

        btn_save = v.findViewById(R.id.btn_save);


        back_ic.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_userProfileFragment_to_mainCustomerFragment));

        edit_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_name.setEnabled(true);
            }
        });

        edit_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_email.setEnabled(true);
            }
        });

        edit_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_pass.setEnabled(true);
            }
        });


        change_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.Companion.with(getActivity())
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Saving...", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
