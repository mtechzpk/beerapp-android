package com.example.beershop.Fragments.UserSideFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.beershop.Adapters.CreditsReceivedAdapter;
import com.example.beershop.Adapters.ViewDetailsAdapter;
import com.example.beershop.Models.CreditsModel;
import com.example.beershop.Models.ViewDetailsModel;
import com.example.beershop.R;

import java.util.ArrayList;

public class CreditsReceivedFragment extends Fragment {
    public CreditsReceivedFragment() {
        // Required empty public constructor
    }

    private View v;
    private ImageView back;
    private RecyclerView credits_received_rv;
    private ArrayList<CreditsModel> creditsModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_credits_received, container, false);

        init();

        return v;
    }

    private void init() {
        back = v.findViewById(R.id.back);
        credits_received_rv = v.findViewById(R.id.credits_received_rv);
        initAdapter();

        back.setOnClickListener(Navigation.
                createNavigateOnClickListener(R.id.action_creditsReceivedFragment_to_mainCustomerFragment));

    }

    private void setEventsName() {
        creditsModel = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            CreditsModel viewDetailsModel = new CreditsModel();
            viewDetailsModel.setCredit_name("Survey" + i);
            viewDetailsModel.setCredit_date("03" + i);
            viewDetailsModel.setCredit_time("04:30" + i);
            creditsModel.add(viewDetailsModel);
        }
    }

    private void initAdapter() {
        setEventsName();
        CreditsReceivedAdapter pAdapter = new CreditsReceivedAdapter(getActivity(), creditsModel);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        credits_received_rv.setLayoutManager(linearLayoutManager);
        credits_received_rv.setAdapter(pAdapter);
    }
}
