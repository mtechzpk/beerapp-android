package com.example.beershop.Fragments;

import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.beershop.Adapters.ViewDetailsAdapter;
import com.example.beershop.Models.ViewDetailsModel;
import com.example.beershop.R;

import java.util.ArrayList;

public class ViewDetailsFragment extends Fragment {
    public ViewDetailsFragment() {
        // Required empty public constructor
    }

    private View view;
    private RecyclerView view_details_rv;
    private ImageView back;
    private ArrayList<ViewDetailsModel> ViewDetailsModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_details, container, false);

        init();

        return view;
    }

    private void init() {
        back=view.findViewById(R.id.back);
        view_details_rv=view.findViewById(R.id.view_details_rv);
        initAdapter();

        back.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_viewDetailsFragment_to_userRecordDetailFragment));
    }

    private void setEventsName() {
        ViewDetailsModel = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            ViewDetailsModel viewDetailsModel = new ViewDetailsModel();
            viewDetailsModel.setCustomer_name("Customer" + i);
            ViewDetailsModel.add(viewDetailsModel);
        }
    }

    private void initAdapter() {
        setEventsName();
        ViewDetailsAdapter pAdapter = new ViewDetailsAdapter(getActivity(), ViewDetailsModel);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        view_details_rv.setLayoutManager(linearLayoutManager);
        view_details_rv.setAdapter(pAdapter);
    }
}
