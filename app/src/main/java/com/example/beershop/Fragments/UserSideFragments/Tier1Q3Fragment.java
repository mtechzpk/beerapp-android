package com.example.beershop.Fragments.UserSideFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;
import com.white.progressview.HorizontalProgressView;

public class Tier1Q3Fragment extends Fragment {
    public Tier1Q3Fragment() {
        // Required empty public constructor
    }
    private View v;
    private TextView mTextField;
    private HorizontalProgressView progress100;
    private MaterialButton btn_next;
    private EditText answer_et;
    private String answer = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_tier1_q3, container, false);
        init();
        return v;
    }

    private void init() {
        progress100 = v.findViewById(R.id.progress100);
        progress100.setTextVisible(false);
        progress100.setReachBarSize(4);
        progress100.setProgressPosition(HorizontalProgressView.TOP);
        progress100.setProgress(66);
        mTextField = v.findViewById(R.id.mTextField);
        answer_et = v.findViewById(R.id.answer_et);
        btn_next = v.findViewById(R.id.btn_next);

        btn_next.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_tier1Q3Fragment_to_tokenFragment));
    }

    private void CountDown() {
        new CountDownTimer(45000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTextField.setText("Remaining Time: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                mTextField.setText("Done!");
            }
        }.start();
    }
}
