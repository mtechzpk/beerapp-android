package com.example.beershop.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.beershop.Adapters.ViewUserRecordAdapter;
import com.example.beershop.Models.ViewUserRecordModel;
import com.example.beershop.R;

import java.util.ArrayList;

public class ViewUserRecordFragment extends Fragment {
    public ViewUserRecordFragment() {
        // Required empty public constructor
    }

    private View view;
    private RecyclerView view_user_record_rv;
    private ImageView back;
    private ArrayList<ViewUserRecordModel> viewUserRecordModels;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_user_record, container, false);
        init();
        initAdapter();
        back.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_viewUserRecordFragment_to_scanCodeFragment));
        return view;
    }

    private void init() {
        back = view.findViewById(R.id.back);
        view_user_record_rv = view.findViewById(R.id.view_user_record_rv);
    }

    private void setEventsName() {
        viewUserRecordModels = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            ViewUserRecordModel viewUserRecordModel = new ViewUserRecordModel();
            viewUserRecordModel.setDate_from("20-01-2020" + i);
            viewUserRecordModel.setDate_to("25-05-2020" + i);
            viewUserRecordModels.add(viewUserRecordModel);
        }
    }

    private void initAdapter() {
        setEventsName();
        ViewUserRecordAdapter pAdapter = new ViewUserRecordAdapter(getActivity(), viewUserRecordModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        view_user_record_rv.setLayoutManager(linearLayoutManager);
        view_user_record_rv.setAdapter(pAdapter);
    }
}
