package com.example.beershop.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beershop.Adapters.ViewDetailsAdapter;
import com.example.beershop.Models.ViewDetailsModel;
import com.example.beershop.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;


public class ScanResultDialogFragment extends DialogFragment {

    private View view;
    TextView btn_ok;
    private RecyclerView view_details_rv;
    private ImageView back;
    private ArrayList<ViewDetailsModel> ViewDetailsModel;

    public static ScanResultDialogFragment newInstance(int title) {
        ScanResultDialogFragment frag = new ScanResultDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.scan_dialog_layout, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }
    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.pdlg_default_animation;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_ok = view.findViewById(R.id.btn_ok);
        init();
//        initAdapter();

        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });

    }
    private void init() {
//        back=view.findViewById(R.id.back);
//        view_details_rv=view.findViewById(R.id.view_details_rv);
//
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().finish();
//            }
//        });
    }

    private void setEventsName() {
        ViewDetailsModel = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            ViewDetailsModel viewDetailsModel = new ViewDetailsModel();
            viewDetailsModel.setCustomer_name("Customer" + i);
            ViewDetailsModel.add(viewDetailsModel);
        }
    }

    private void initAdapter() {
        setEventsName();
        ViewDetailsAdapter pAdapter = new ViewDetailsAdapter(getActivity(), ViewDetailsModel);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        view_details_rv.setLayoutManager(linearLayoutManager);
        view_details_rv.setAdapter(pAdapter);
    }
}