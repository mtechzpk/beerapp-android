package com.example.beershop.Fragments.UserSideFragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.beershop.Activities.SimpleFragmentActivity;
import com.example.beershop.Activities.SurveyTestActivity;
import com.example.beershop.Activities.UserSide.CustomerLandingActivity;
import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;
import com.surveymonkey.surveymonkeyandroidsdk.SurveyMonkey;
import com.surveymonkey.surveymonkeyandroidsdk.utils.SMError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_OK;

public class TakeSurveyFragment extends Fragment {

    public static final int SM_REQUEST_CODE = 0;
    public static final String SM_RESPONDENT = "smRespondent";
    public static final String SM_ERROR = "smError";
    public static final String RESPONSES = "responses";
    public static final String QUESTION_ID = "question_id";
    public static final String FEEDBACK_QUESTION_ID = "813797519";
    public static final String ANSWERS = "answers";
    public static final String ROW_ID = "row_id";
    public static final String FEEDBACK_FIVE_STARS_ROW_ID = "9082377273";
    public static final String FEEDBACK_POSITIVE_ROW_ID_2 = "9082377274";
    public static final String SAMPLE_APP = "Sample App";
//    public static final String SURVEY_HASH="";
//    public static final String SURVEY_HASH = "6WJ2KQR";

    private SurveyMonkey s = new SurveyMonkey();
    public TakeSurveyFragment() {
        // Required empty public constructor
    }
    private View v;
    private CardView survey1, survey2, survey3;
private ImageView img_lock1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_take_survey, container, false);
        init();
        return v;
    }

    private void init() {
        survey1 = v.findViewById(R.id.survey1);
        survey2 = v.findViewById(R.id.survey2);
        survey3 = v.findViewById(R.id.survey3);
        img_lock1 = v.findViewById(R.id.img_lock1);
        if (CustomerLandingActivity.survay1==true){
            img_lock1.setBackgroundResource(R.drawable.beer_ic);
        }else {
            img_lock1.setBackgroundResource(R.drawable.ic_lock);

        }
//        survey1.setOnClickListener(Navigation.
//                createNavigateOnClickListener(R.id.action_takeSurveyFragment_to_tierOneFragment));

        survey1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), SurveyTestActivity.class);
//                startActivity(intent);
                String SURVEY_HASH = "6WJ2KQR";
                takeSurvey(SURVEY_HASH,0);
            }
        });
        survey2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CustomerLandingActivity.survay1==true){
                    String SURVEY_HASH = "ZFWXRTM";
                    takeSurvey(SURVEY_HASH,1);

                }else {
                    Toast.makeText(getActivity(), "Complete Your first Survey", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        survey2.setOnClickListener(Navigation.
//                createNavigateOnClickListener(R.id.action_mainCustomerFragment_to_takeSurveyFragment));
//        survey3.setOnClickListener(Navigation.
//                createNavigateOnClickListener(R.id.action_takeSurveyFragment_to_tierThreeFragment));


        survey3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void takeSurvey(String SURVEY_HASH,int code) {
        //This is how you display a survey for the user to take
        // Remember: you must supply the parent activity (e.g. this), your own request code (to differentiate from other activities), and the collector hash of the SDK collector you've created at SurveyMonkey.com
        s.startSMFeedbackActivityForResult(getActivity(), code, SURVEY_HASH);
    }

    public void takeSurveyFragment(View view) {
        //This is how you display a survey for the user to take
        // Remember: you must supply the parent activity (e.g. this), your own request code (to differentiate from other activities), and the collector hash of the SDK collector you've created at SurveyMonkey.com
//        SimpleFragmentActivity.startActivity(getActivity(), SURVEY_HASH);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
