package com.example.beershop.Fragments.UserSideFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;
import com.white.progressview.HorizontalProgressView;

public class Tier2Q2Fragment extends Fragment {
    public Tier2Q2Fragment() {
        // Required empty public constructor
    }
    private View v;
    private HorizontalProgressView progress100;
    private MaterialButton btn_next;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=  inflater.inflate(R.layout.fragment_tier2_q2, container, false);
        init();
        return v;
    }

    private void init() {

        progress100 = v.findViewById(R.id.progress100);
        btn_next = v.findViewById(R.id.btn_next);

        btn_next.setOnClickListener(Navigation.
                createNavigateOnClickListener(R.id.action_tier2Q2Fragment_to_token2Fragment));

// for example:
        progress100.setTextVisible(false);
        progress100.setReachBarSize(4);
        progress100.setProgressPosition(HorizontalProgressView.TOP);
        progress100.setProgress(50);

    }
}
