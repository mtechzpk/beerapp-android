package com.example.beershop.Fragments;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beershop.R;

public class UserRecordDetailFragment extends Fragment {
    public UserRecordDetailFragment() {
        // Required empty public constructor
    }
    private View view;
    private ImageView back;
    private TextView total_beer, total_price, send_invice_to_admin, view_details;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_record_detail, container, false);
        init();

        return view;
    }

    private void init() {
        back = view.findViewById(R.id.back);

        total_beer = view.findViewById(R.id.total_beer);
        total_price = view.findViewById(R.id.total_price);
        send_invice_to_admin = view.findViewById(R.id.send_invice_to_admin);
        view_details = view.findViewById(R.id.view_details);
        view_details = view.findViewById(R.id.view_details);

        back.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_userRecordDetailFragment_to_viewUserRecordFragment3));

        send_invice_to_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         showFreeTrialDialog();
            }
        });

        view_details.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_userRecordDetailFragment_to_viewDetailsFragment));

    }
    private void showFreeTrialDialog() {
        DialogFragment newFragment = invoiceDetailDialogFragment.newInstance(
                R.string.search_word_meaning_dialog_title);
        newFragment.show(getFragmentManager(), "dialog");

    }
}
