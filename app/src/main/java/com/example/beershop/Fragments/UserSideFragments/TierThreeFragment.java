package com.example.beershop.Fragments.UserSideFragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.beershop.R;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.button.MaterialButton;

public class TierThreeFragment extends Fragment {
    public TierThreeFragment() {
        // Required empty public constructor
    }

    private View v;
    private MaterialButton upload_snapshot, pay_now;
    private String URL = "https://www.nj.betmgm.com/en/mobileportal/register?rurl=https:%2F%2Fsports.nj.betmgm.com%2Fen%2Fsports";
    private String Image_uri = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_tier_three, container, false);

        init();
        return v;
    }

    private void init() {
        upload_snapshot = v.findViewById(R.id.upload_snapshot);
        pay_now = v.findViewById(R.id.pay_now);

        pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(URL); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

                Toast.makeText(getActivity(), "Pay Now", Toast.LENGTH_SHORT).show();
            }
        });

        upload_snapshot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Upload Snapshot", Toast.LENGTH_SHORT).show();
                ImagePicker.Companion.with(getActivity())
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri imageUri = data.getData();
//            profile_img.setImageURI(imageUri);

            Image_uri = imageUri.toString();

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(getActivity(), ImagePicker.RESULT_ERROR, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }
}
