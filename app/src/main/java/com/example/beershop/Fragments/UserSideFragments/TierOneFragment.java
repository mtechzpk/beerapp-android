package com.example.beershop.Fragments.UserSideFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;
import com.white.progressview.HorizontalProgressView;

public class TierOneFragment extends Fragment {
    public TierOneFragment() {
        // Required empty public constructor
    }

    private View v;
    private TextView mTextField;
    private HorizontalProgressView progress100;
    private MaterialButton btn_next;
    private EditText answer_et;
    private String answer = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_tier_one, container, false);

        init();
        return v;
    }

    private void init() {
        progress100 = v.findViewById(R.id.progress100);
        progress100.setTextVisible(false);
        progress100.setReachBarSize(4);
        progress100.setProgressPosition(HorizontalProgressView.TOP);
        mTextField = v.findViewById(R.id.mTextField);
        answer_et = v.findViewById(R.id.answer_et);
        btn_next = v.findViewById(R.id.btn_next);

//        CountDown();

//        answer_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    //do something
//                    answer = answer_et.getText().toString();
//                    if (answer.equals("")) {
//                        Toast.makeText(getActivity(), "Please answer the question first", Toast.LENGTH_SHORT).show();
//                    } else if (answer.equals("6")) {
//                        Toast.makeText(getActivity(), "If you don't bet Bet4Beers isn't for you. But thats OK Have a free beer on us anyway.", Toast.LENGTH_SHORT).show();
//                    } else if (answer.equals("1")) {
//                        Toast.makeText(getActivity(), "Sports Betting", Toast.LENGTH_SHORT).show();
//                    } else if (answer.equals("2")) {
//                        Toast.makeText(getActivity(), "Casino", Toast.LENGTH_SHORT).show();
//                    } else if (answer.equals("3")) {
//                        Toast.makeText(getActivity(), "Poker", Toast.LENGTH_SHORT).show();
//                    } else if (answer.equals("4")) {
//                        Toast.makeText(getActivity(), "Fantasy", Toast.LENGTH_SHORT).show();
//                    } else if (answer.equals("5")) {
//                        Toast.makeText(getActivity(), "Horse Racing", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getActivity(), "Invalid answer", Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//                return false;
//            }
//        });

//        answer = answer_et.getText().toString();
//        if (answer.equals("")) {
//            Toast.makeText(getActivity(), "Please answer first question", Toast.LENGTH_SHORT).show();
//        } else if (answer.equals("6")) {
//            Toast.makeText(getActivity(), "If you don't bet Bet4Beers isn't for you. But thats OK Have a free beer on us anyway.", Toast.LENGTH_SHORT).show();
//        } else {
//            btn_next.setOnClickListener(Navigation.
//                    createNavigateOnClickListener(R.id.action_tierOneFragment_to_tierTwoFragment));
//        }

//        if (answer.equals("")) {
//            btn_next.setOnClickListener(Navigation.
//                    createNavigateOnClickListener(R.id.action_tierOneFragment_to_tier1Q2Fragment));
//        }

        btn_next.setOnClickListener(Navigation.
                createNavigateOnClickListener(R.id.action_tierOneFragment_to_tier1Q2Fragment));
    }

    private void CountDown() {
        new CountDownTimer(45000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTextField.setText("Remaining Time: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                mTextField.setText("Done!");
            }
        }.start();
    }

}
