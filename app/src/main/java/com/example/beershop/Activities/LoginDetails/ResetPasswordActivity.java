package com.example.beershop.Activities.LoginDetails;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;

public class ResetPasswordActivity extends AppCompatActivity {
    private EditText user_email;
    private MaterialButton btn_reset;
    private String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        init();
    }

    private void init() {
        user_email = findViewById(R.id.user_email);
        btn_reset = findViewById(R.id.btn_reset);

        email = user_email.getText().toString().trim();
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (!email.equals("")) {
//                    if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//                        Toast.makeText(ResetPasswordActivity.this, "Email:"+email, Toast.LENGTH_SHORT).show();
//                    }else {
//                        user_email.setError("Invalid Email");
//                    }
//                }else {
//                    user_email.setError("Email Required");
//                }
                Toast.makeText(ResetPasswordActivity.this, "Email:"+email, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
