package com.example.beershop.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.beershop.Activities.LoginDetails.LoginActivity;
import com.example.beershop.Activities.Utilities.Utilities;
import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;

public class LoginAsActivity extends AppCompatActivity {
    private MaterialButton btn_as_admin, btn_as_customer;
    private String loginAs = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_as);

        init();
    }

    private void init() {
        btn_as_admin = findViewById(R.id.btn_as_admin);
        btn_as_customer = findViewById(R.id.btn_as_customer);


        btn_as_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAs = "admin";
                Intent intent = new Intent(LoginAsActivity.this, LoginActivity.class);
                Utilities.saveString(LoginAsActivity.this,"loginAs",loginAs);
                startActivity(intent);

            }
        });

        btn_as_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAs = "customer";
                Intent intent = new Intent(LoginAsActivity.this, LoginActivity.class);
                Utilities.saveString(LoginAsActivity.this,"loginAs",loginAs);
                startActivity(intent);

            }
        });
    }
}
