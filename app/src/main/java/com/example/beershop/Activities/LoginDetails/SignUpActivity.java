package com.example.beershop.Activities.LoginDetails;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beershop.R;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.button.MaterialButton;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpActivity extends AppCompatActivity {
    private EditText user_email, user_pass, user_c_pass, user_name;
    private MaterialButton btn_signup;
    private CircleImageView profile_img;
    private TextView login,upload;
    private CheckBox cbRememberme;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private String full_name = "", email = "", password = "", c_password = "";
    String dob;
    private TextView profile_img_name, tvDOB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
    }

    private void init() {
        user_email = findViewById(R.id.user_email);
        user_pass = findViewById(R.id.user_pass);
        user_c_pass = findViewById(R.id.user_c_pass);
        user_name = findViewById(R.id.user_name);

        btn_signup = findViewById(R.id.btn_signup);
        profile_img = findViewById(R.id.profile_img);
        login = findViewById(R.id.login);
        upload = findViewById(R.id.upload);
        tvDOB = findViewById(R.id.tvDOB);
        cbRememberme = findViewById(R.id.cbRememberme);

        String text = "By checking this box and clicking Sign Up you agree to the Bet4Beers Privacy Policy and T&Cs";
        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(SignUpActivity.this, "Privacy Policy", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setColor(Color.BLUE);
                textPaint.setUnderlineText(true);
            }
        };

        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(SignUpActivity.this, "T&Cs", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setColor(Color.BLUE);
                textPaint.setUnderlineText(true);
            }
        };

        ss.setSpan(clickableSpan1, 69, 83, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 88, 92, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        cbRememberme.setText(ss);
        cbRememberme.setMovementMethod(LinkMovementMethod.getInstance());

        tvDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        SignUpActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
//                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                dob = year + "-" + month + "-" + day;
                tvDOB.setText(dob);
            }
        };
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.Companion.with(SignUpActivity.this)
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                full_name = user_name.getText().toString().trim();
                email = user_email.getText().toString().trim();
                password = user_pass.getText().toString().trim();
                c_password = user_c_pass.getText().toString().trim();

//                if (!full_name.equals("")){
//                    if (!email.equals("")){
//                        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()){
//                            if (!password.equals("")){
//                                if (!c_password.equals("")){
//                                    if (password.equals(c_password)){
//                                        Toast.makeText(SignUpActivity.this, "Everything Okay", Toast.LENGTH_SHORT).show();
//                                    }else {
//                                        user_c_pass.setError("Password not matches");
//                                    }
//                                }else {
//                                    user_c_pass.setError("Confirm Password Required");
//                                }
//                            }else{
//                                user_pass.setError("Password Required");
//                            }
//                        }else{
//                            user_email.setError("Invalid Email");
//                        }
//                    }else {
//                        user_email.setError("Email Required");
//                    }
//                }else{
//                    user_name.setError("Name Required");
//                }
                Toast.makeText(SignUpActivity.this, "Signing Up...", Toast.LENGTH_SHORT).show();
            }
        });

        profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.Companion.with(SignUpActivity.this)
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri imageUri = data.getData();
            profile_img.setImageURI(imageUri);

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.RESULT_ERROR, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }
}
