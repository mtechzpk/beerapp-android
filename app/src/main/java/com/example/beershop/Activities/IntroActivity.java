package com.example.beershop.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;

public class IntroActivity extends AppCompatActivity {
    private MaterialButton lets_go;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        init();
    }

    private void init() {
        lets_go = findViewById(R.id.lets_go);

        lets_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainIntent = new Intent(IntroActivity.this, LoginAsActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });
    }
}
