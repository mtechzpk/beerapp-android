package com.example.beershop.Activities.UserSide;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.beershop.Activities.Utilities.Utilities;
import com.example.beershop.Fragments.UserSideFragments.CreditsReceivedFragment;
import com.example.beershop.Fragments.UserSideFragments.Token2Fragment;
import com.example.beershop.Fragments.UserSideFragments.TokenFragment;
import com.example.beershop.MainActivity;
import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;
import com.surveymonkey.surveymonkeyandroidsdk.utils.SMError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class CustomerLandingActivity extends AppCompatActivity {
    public NavController navController;
    ImageView ivDrawer, ivNotification;
    private DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout rlToolbar;
    public static final int SM_REQUEST_CODE = 0;
    public static final String SM_RESPONDENT = "smRespondent";
    public static final String SM_ERROR = "smError";
    public static final String RESPONSES = "responses";
    public static final String QUESTION_ID = "question_id";
    public static final String FEEDBACK_QUESTION_ID = "463173613";
    public static final String ANSWERS = "answers";
    public static final String ROW_ID = "row_id";
    public static final String FEEDBACK_FIVE_STARS_ROW_ID = "3063565052";
    public static final String FEEDBACK_POSITIVE_ROW_ID_2 = "3063565051";
    public static boolean survay1 = false;
    public static final String SAMPLE_APP = "Sample App";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_landing);

        drawer = findViewById(R.id.user_drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ivDrawer = findViewById(R.id.menu_ic);
        rlToolbar = findViewById(R.id.rvToolbar);
        setColor();

        initNavigation();
    }

    private void initNavigation() {
        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        navController = Navigation.findNavController(this, R.id.nav_user_host_fragment);
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupWithNavController(navigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                    rlToolbar.setVisibility(View.VISIBLE);
//                    tvTitle.setText(destination.getLabel());
                }

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //This is where you consume the respondent data returned by the SurveyMonkey Mobile Feedback SDK
        //In this example, we deserialize the user's response, check to see if they gave our app 4 or 5 stars, and then provide visual prompts to the user based on their response
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {
            boolean isPromoter = false;
            boolean isPromoter2 = false;
            if (requestCode == SM_REQUEST_CODE) {
                try {
                    String respondent = intent.getStringExtra("smRespondent");
                    Log.d("SM", respondent);
                    JSONObject surveyResponse = new JSONObject(respondent);
                    JSONArray responsesList = surveyResponse.getJSONArray("responses");
                    JSONObject response;
                    JSONArray answers;
                    JSONObject currentAnswer;

                    for (int i = 0; i < responsesList.length(); i++) {
                        response = responsesList.getJSONObject(i);
                        if (response.getString(QUESTION_ID).equals(FEEDBACK_QUESTION_ID)) {
                            answers = response.getJSONArray(ANSWERS);
                            for (int j = 0; j < answers.length(); j++) {
                                currentAnswer = answers.getJSONObject(j);
                                if (currentAnswer.getString(ROW_ID).equals(FEEDBACK_FIVE_STARS_ROW_ID) || currentAnswer.getString(ROW_ID).equals(FEEDBACK_POSITIVE_ROW_ID_2)) {
                                    isPromoter = true;
                                    survay1 = true;
                                    TokenFragment tokenFragment = new TokenFragment();
                                    this.getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.framelayout, tokenFragment, tokenFragment.getClass().getSimpleName()).addToBackStack(TokenFragment.class.getName()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();


                                    break;

                                }
                            }
                            if (isPromoter) {
                                break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.getStackTraceString(e);
                }
                if (isPromoter) {
                    Toast t = Toast.makeText(this, getString(R.string.promoter_prompt), Toast.LENGTH_LONG);
                    t.show();
                } else {
                    Toast t = Toast.makeText(this, getString(R.string.detractor_prompt), Toast.LENGTH_LONG);
                    t.show();
                }
            }
            if (requestCode == 1) {
                try {
                    String respondent = intent.getStringExtra("smRespondent");
                    Log.d("SM", respondent);
                    JSONObject surveyResponse = new JSONObject(respondent);
                    JSONArray responsesList = surveyResponse.getJSONArray("responses");
                    JSONObject response;
                    JSONArray answers;
                    JSONObject currentAnswer;

                    for (int i = 0; i < responsesList.length(); i++) {
                        response = responsesList.getJSONObject(i);
                        if (response.getString(QUESTION_ID).equals("463187451")) {
                            answers = response.getJSONArray(ANSWERS);
                            for (int j = 0; j < answers.length(); j++) {
                                currentAnswer = answers.getJSONObject(j);
                                if (currentAnswer.getString(ROW_ID).equals("3063662059") || currentAnswer.getString(ROW_ID).equals("3063662058")) {
                                    isPromoter2 = true;
                                    survay1 = true;
                                    Token2Fragment creditsReceivedFragment = new Token2Fragment();
                                    this.getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.framelayout, creditsReceivedFragment, creditsReceivedFragment.getClass().getSimpleName()).addToBackStack(Token2Fragment.class.getName()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();


                                    break;

                                }
                            }
                            if (isPromoter2) {
                                break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.getStackTraceString(e);
                }
                if (isPromoter2) {
                    Toast t = Toast.makeText(this, getString(R.string.promoter_prompt), Toast.LENGTH_LONG);
                    t.show();
                } else {
                    Toast t = Toast.makeText(this, getString(R.string.detractor_prompt), Toast.LENGTH_LONG);
                    t.show();
                }
            }

        } else {
            Toast t = Toast.makeText(this, getString(R.string.error_prompt), Toast.LENGTH_LONG);
            t.show();
            SMError e = (SMError) intent.getSerializableExtra(SM_ERROR);
            Log.d("SM-ERROR", e.getDescription());
        }
    }

    public void setColor() {

        Menu menu = navigationView.getMenu();
        MenuItem tools1 = menu.findItem(R.id.mainCustomerFragment);
        SpannableString s1 = new SpannableString(tools1.getTitle());
        s1.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s1.length(), 0);
        tools1.setTitle(s1);

        MenuItem tools2 = menu.findItem(R.id.userProfileFragment);
        SpannableString s2 = new SpannableString(tools2.getTitle());
        s2.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s2.length(), 0);
        tools2.setTitle(s2);

        MenuItem menuItem = menu.findItem(R.id.creditsReceivedFragment);
        SpannableString s7 = new SpannableString(menuItem.getTitle());
        s7.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s7.length(), 0);
        menuItem.setTitle(s7);

        MenuItem menuItem1 = menu.findItem(R.id.creditsRemainingFragment);
        SpannableString s5 = new SpannableString(menuItem1.getTitle());
        s5.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s5.length(), 0);
        menuItem1.setTitle(s5);

        MenuItem menuItem2 = menu.findItem(R.id.orderHistoryFragment);
        SpannableString s6 = new SpannableString(menuItem2.getTitle());
        s6.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s6.length(), 0);
        menuItem2.setTitle(s6);

//        Menu menu3 = navigationView.getMenu();
//        MenuItem tools3= menu3.findItem(R.id.nav_rate_us);
//        SpannableString s3 = new SpannableString(tools3.getTitle());
//        s3.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s3.length(), 0);
//        tools3.setTitle(s3);

        MenuItem tools4 = menu.findItem(R.id.logout);
        SpannableString s4 = new SpannableString(tools4.getTitle());
        s4.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s4.length(), 0);
        tools4.setTitle(s4);

        MenuItem itmemenu = menu.findItem(R.id.refferals);
        SpannableString sReferrals = new SpannableString(itmemenu.getTitle());
        sReferrals.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, sReferrals.length(), 0);
        itmemenu.setTitle(sReferrals);

    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.nav_menu, menu);

        // change color for icon 0
        Drawable yourdrawable = menu.getItem(0).getIcon(); // change 0 with 1,2 ...
        yourdrawable.mutate();
        yourdrawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
        } else if (!navController.getCurrentDestination().getLabel().toString().equals("fragment_main_customer")) {
            super.onBackPressed();
        } else {
            showCustomDialog1();
        }
    }

    private void showCustomDialog1() {
        final PrettyDialog pDialog = new PrettyDialog(CustomerLandingActivity.this);
        pDialog
                .setTitle("Exit")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.colorPrimary,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finishAffinity();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();
    }

}
