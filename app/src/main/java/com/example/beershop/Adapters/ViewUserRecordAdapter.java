package com.example.beershop.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beershop.MainActivity;
import com.example.beershop.Models.ViewUserRecordModel;
import com.example.beershop.R;

import java.util.ArrayList;


public class ViewUserRecordAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ViewUserRecordModel> ViewUserRecordModel;

    public ViewUserRecordAdapter(Context context, ArrayList<ViewUserRecordModel> ViewUserRecordModel) {
        this.context = context;
        this.ViewUserRecordModel = ViewUserRecordModel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_user_record_row, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.rlItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) context).navController.navigate(R.id.action_viewUserRecordFragment_to_userRecordDetailFragment);

            }
        });

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return ViewUserRecordModel.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tv_date_from,tv_date_to;
RelativeLayout rlItems;
        private BookViewHolder(View itemView) {
            super(itemView);

            //init views
            tv_date_from    = itemView.findViewById(R.id.tv_date_from);
            tv_date_to      = itemView.findViewById(R.id.tv_date_to);
            rlItems      = itemView.findViewById(R.id.rlItems);
        }

        private void bind(int pos) {
            ViewUserRecordModel eventsModel = ViewUserRecordModel.get(pos);
            tv_date_from.setText(eventsModel.getDate_from());
            tv_date_to  .setText(eventsModel.getDate_to());
        }


    }

}
