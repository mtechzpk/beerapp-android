package com.example.beershop.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beershop.Models.ViewDetailsModel;
import com.example.beershop.R;

import java.util.ArrayList;

public class ViewDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ViewDetailsModel> ViewDetailsModel;

    public ViewDetailsAdapter(Context context, ArrayList<ViewDetailsModel> ViewDetailsModel) {
        this.context = context;
        this.ViewDetailsModel = ViewDetailsModel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_details_row, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return ViewDetailsModel.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tv_customer_name,tv_customer_id,tv_service_type;

        private BookViewHolder(View itemView) {
            super(itemView);

            //init views
            tv_customer_name    = itemView.findViewById(R.id.customer_name);
            tv_customer_id      = itemView.findViewById(R.id.customer_id);
            tv_service_type     = itemView.findViewById(R.id.service_type);
        }

        private void bind(int pos) {
            ViewDetailsModel eventsModel = ViewDetailsModel.get(pos);
            tv_customer_name.setText(eventsModel.getCustomer_name());
            tv_customer_id  .setText(eventsModel.getCustomer_id());
            tv_service_type .setText(eventsModel.getService_type());
        }

    }
}

